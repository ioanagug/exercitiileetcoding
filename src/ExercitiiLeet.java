public class ExercitiiLeet {
    public static void main(String[] args) {
        String roman = "CMXLVIII";
        String needle = "abc";
        String haystack = "defcxm";
        System.out.println(romanToInt(roman));
        System.out.println(strStr(haystack, needle));
    }

    public static int romanToInt(String s) {
        int[] sum = new int[s.length()];
        int nrArab = 0;
        for (int i = 0; i < s.length(); i++) {
            switch (s.charAt(i)) {
                case 'I': {
                    sum[i] = 1;
                    break;
                }
                case 'V': {
                    sum[i] = 5;
                    if ((i >= 1) && (sum[i - 1] == 1)) {
                        sum[i] = 4;
                        sum[i - 1] = 0;
                    }
                    break;
                }
                case 'X': {
                    sum[i] = 10;
                    if ((i >= 1) && (sum[i - 1] == 1)) {
                        sum[i] = 9;
                        sum[i - 1] = 0;
                    }
                    break;
                }
                case 'L': {
                    sum[i] = 50;
                    if ((i >= 1) && (sum[i - 1] == 10)) {
                        sum[i] = 40;
                        sum[i - 1] = 0;
                    }
                    break;
                }
                case 'C': {
                    sum[i] = 100;
                    if ((i >= 1) && (sum[i - 1] == 10)) {
                        sum[i] = 90;
                        sum[i - 1] = 0;
                    }
                    break;
                }
                case 'D': {
                    sum[i] = 500;
                    if ((i >= 1) && (sum[i - 1] == 100)) {
                        sum[i] = 400;
                        sum[i - 1] = 0;
                    }
                    break;
                }
                case 'M': {
                    sum[i] = 1000;
                    if ((i >= 1) && (sum[i - 1] == 100)) {
                        sum[i] = 900;
                        sum[i - 1] = 0;
                    }
                    break;
                }
            }
        }
        for (int i = 0; i < sum.length; i++) {
            nrArab = nrArab + sum[i];
        }
        return nrArab;
    }

    public static int strStr(String haystack, String needle) {
        int poz = -1;
        if (haystack.contains(needle)) {
            for (int i = 0; i < haystack.length(); i++) {
                String sub=haystack.substring(i, (i + needle.length()));
                if (needle.equals(sub)) {
                    poz = i;
                    break;
                }
            }
        }
        if (haystack.equals("") && (needle.equals(""))) {
            poz = 0;
        }
        return poz;
    }
}
