public class EcercitiiLeet2 {
    public static void main(String[] args) {
        int[] sir = {-3,6,2,5,8,6};
        System.out.println(minStartValue(sir));
    }

    public static int minStartValue(int[] nums) {
        int startValue = 0;
        int sumaAct;
        int min=0;

        while (min<1) {
            startValue++;
            sumaAct=startValue+nums[0];
            min=sumaAct;
            for (int i=1; i< nums.length; i++) {
                sumaAct=sumaAct+nums[i];
                if (sumaAct<min) {
                    min=sumaAct;
                }
            }
        }

        return startValue;
    }
}
